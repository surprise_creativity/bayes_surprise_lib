# BayesSurprise : Biblioteca de Surpresa Bayesiana #

*Implementação de Surpresa Bayesiana para aplicações de Criatividade Computacional baseado em [Baldi, P. e Itti, L (2010)](http://ilab.usc.edu/publications/doc/Baldi_Itti10nn.pdf).*

### Conteúdo deste repositório: ###

* Projeto na [IDE Eclipse](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/keplersr1) em linguagem de programação Java (compilador JavaSE 1.8).

### Dependência: ###

* **[Colt Library](https://dst.lbl.gov/ACSSoftware/colt/)**:
    * Biblioteca Open Source em linguagem de programação Java;
    * Computação científica de alto desempenho;
    * Desenvolvido pelo CERN.

### Como configurar o repositório no Linux (Ubuntu): ###

1. Clonar o repositório:
    * Abra o console de comando do sistema operacional e navegue até o local onde será clonado o repositório;
    * Abra a pagina de [`Overview`](https://bitbucket.org/surprise_creativity/bayes_surprise_lib/overview) deste repositório, copie o link HTTPS no topo da pagina e execute os comandos abaixo;
    * `git clone link_HTTPS_copiado BayesSurprise`;
    * Digite sua senha do Bitbucket quando exigido e `Enter`.
2. Adicionar a **Colt Library** ao projeto:
    * Com o console aberto, navegue e entre na pasta `BayesSurprise/libs/` do repositório clonado;
    * Em um navegador, visite a pagina [oficial do projeto Colt](https://dst.lbl.gov/ACSSoftware/colt/), vá no menu [`Download`](https://dst.lbl.gov/ACSSoftware/colt/colt-download/) e clique no link do diretório [`releases/`](https://dst.lbl.gov/ACSSoftware/colt/colt-download/releases/);
    * Na pagina de [`releases`](https://dst.lbl.gov/ACSSoftware/colt/colt-download/releases/) do projeto Colt, escolha um arquivo `.zip` de versão igual ou superior à `1.2.0` (e.g. `colt-1.2.0.zip`) e copie o link deste arquivo (e.g. `https://dst.lbl.gov/ACSSoftware/colt/colt-download/releases/colt-1.2.0.zip`);
    * Novamente no console, execute os comandos abaixo;
    * `wget -c link_copiado_colt`;
    * `unzip colt-*`;
    * `rm colt-*`.
3. Importar o projeto na IDE Eclipse:
    * No menu `File`, clique no sub-menu `Import`;
    * Na janela de seleção, dentro da pasta `General`, dê um duplo-clique na opção `Projects from Folder or Archive`;
    * Na janela de seleção da fonte de importação, clique em `Diretory...` e selecione no seu sistemas de arquivo a pasta onde foi clonado o repositório (i.e. a pasta `BayesSurprise`).
    * Mantenha os dois checkbox, `Search for nested projects` e `Detect and configure project natures`, marcados e pressione o botão `Finish`.

### Como utilizar a biblioteca: ###

No arquivo `BayesSurprise/src/Exemplo.java` contém todos os detalhes de utilização da biblioteca. Abaixo segue alguns exemplos básicos dentre os contidos nesse arquivo.

* Organização dos pacotes:
    * O pacote raiz é o `surp`;
    * O pacote `surp.artifact` contém a abstração de um artefato de Criatividade Computacional;
    * O pacote `surp.evaluation` contém classes e interfaces para avaliação da surpresa de artefatos;
    * O pacote `surp.model` contém classes e interfaces para abstração de modelos de Surpresa Bayesiana;
    * O pacote `surp.model.distribuition` contém classes que implementam os modelos de Surpresa Bayesiana para diferentes tipos de dados;
* Criando um artefato de Criatividade Computacional:
```java
  import surp.artifact.ObjectArtifact;
  
  //...
  
  ObjectArtifact objArt = new ObjectArtifact();
  // Adicionando valores de atributos ao artefato:
  objArt.add(true); // Booleanos
  objArt.add(2);    // Inteiros maiores que 0
  objArt.add(2.0);  // Reais
  //...
  objArt.add(5.0);
```
* Criando um modelo para avaliação de Surpresa Bayesiana:
```java
  // Modelo para artefato:
  import surp.model.ObjectModel;
  // Modelos para atributos:
  // Booleano
  import surp.model.distribution.BetaDistribution;
  // Inteiro (maior que 0)
  import surp.model.distribution.PoissonDistribution;
  // Real (variância fixa)
  import surp.model.distribution.GaussianDistribution;
  // Real (média fixa)
  import surp.model.distribution.InvGammaDistribution;
  // Real (livre)
  import surp.model.distribution.GssInvGammaDistribution;

  //...
  
  ObjectModel objMdl = new ObjectModel();

  // Booleano com média a priori de 50% true
  BetaDistribution boolDist = new BetaDistribution(0.5);
  // Inteiro com média a priori de 2.0
  PoissonDistribution intDist = new PoissonDistribution(2.0);
  // Real com variância fixa em 2.0
  GaussianDistribution doubleDistFixVar = new GaussianDistribution(2.0);
  // Real com média fixa em 3.0
  InvGammaDistribution doubleDistFixMean = new InvGammaDistribution(3.0);
  // Real com média e variância livres
  GssInvGammaDistribution doubleDist = new GssInvGammaDistribution();
  // Obs.: Cada atributo deve ter seu próprio objeto de distribuição,
  //       mesmo que seja do mesmo tipo de dado.
		
  // Adição dos modelos Bayesianos ao modelo de artefato
  // (mesma ordem dos tipos de atributos nos artefatos): 
  objMdl.add(boolDist);
  objMdl.add(intDist);
  objMdl.add(doubleDistFixVar);
  //...
  objMdl.add(doubleDist);

  //...

  // Gravando os DADOS do modelo em arquivo:
  objMdl.write("modelos.dat");
  
  //...

  // Lendo os DADOS do modelo a partir de arquivo:
  objMdl.read("modelos.dat");
  // Obs.: a escrita não persiste o número e tipo dos atributos. Dessa
  //       forma, antes de usar 'read' o objeto de modelo, no caso
  //       'objMdl', já deve ter sido criado e inicializado com
  //       distribuições dos tipos corretos.
```
* Treinando e avaliando os artefatos através dos modelos:
```java
  // Classe estática de avaliação
  import surp.evaluation.ArtifactEval;
  // Compõe a surpresa dos atributos em um único valor
  import surp.evaluation.SimpleIntegrater;
  // Obs.: pode ser implementado um agregador mais complexo que 
  //       'SimpleIntegrater' via interface 'surp.evaluation.Integrater'.

  // ...

  // Agregador que apenas soma a surpresa dos atributos:
  SimpleIntegrater itg = new SimpleIntegrater();

  // Avaliando a compatibilidade dos tipos entre o modelo e o artefato:
  if(ArtifactEval.isCompatible(objMdl, objArt)){
     System.out.println("\n\nFormato compatível!\n\n");
  }

  //...

  // Treinando o modelo através de um artefato:
  ArtifactEval.learn(objMdl, objArt);
  // Obs.: os artefatos não precisam ser persistidos num 'data set', 
  //       porque os modelos são iterativos. Dessa forma, uma vez 
  //       usado 'learn' o modelo apreendeu o artefato.

  //...

  // Avaliando a surpresa de um artefato:
  double surprise = ArtifactEval.getSurprise(objMdl, objArt, itg);
  System.out.println("Surpresa do artefato: " + surprise + "\n");
```

### Contato: ###

Para perguntas, sugestões ou reportar erros nos contate por Email:

* Willian A.S. (Will1Dexter@hotmail.com)

### Licença: ###

No arquivo `BayesSurprise/License.txt` e no topo de cada código fonte em `BayesSurprise/src/` contém um banner, que não pode ser removido, com informações sobre a licença de uso desta biblioteca.

Fique a vontade para nos contatar ou contribuir com este projeto.