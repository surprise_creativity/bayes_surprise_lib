/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: Digamma.java   Implementação da função digamma.              */
/* --------------------------------------------------------------------------------- */

package surp;

/**
 * Tradução da implementação em ANSI-C para Java da função digamma.
 * 
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

/*************************************
An ANSI-C implementation of the digamma-function for real arguments based
on the Chebyshev expansion proposed in appendix E of 
http://arXiv.org/abs/math.CA/0403344 . This is identical to the implementation
by Jet Wimp, Math. Comp. vol 15 no 74 (1961) pp 174 (see Table 1).
For other implementations see
the GSL implementation for Psi(Digamma) in
http://www.gnu.org/software/gsl/manual/html_node/Psi-_0028Digamma_0029-Function.html

Richard J. Mathar, 2005-11-24
**************************************/

public class Digamma {
	/** The constant Pi in high precision */
	private static double M_PIl = 3.1415926535897932384626433832795029;
	
	/** Euler's constant in high precision */
	private static double M_GAMMAl = 0.5772156649015328606065120900824024;

	/** the natural logarithm of 2 in high precision */
	private static double M_LN2l = 0.6931471805599453094172321214581766;

	/** Just for your information, the following lines contain
	* the Maple source code to re-generate the table that is
	* eventually becoming the Kncoe[] array below
	* interface(prettyprint=0) :
	* Digits := 63 :
	* r := 0 :
	* 
	* for l from 1 to 60 do
	* 	d := binomial(-1/2,l) :
	* 	r := r+d*(-1)^l*(Zeta(2*l+1) -1) ;
	* 	evalf(r) ;
	* 	print(%,evalf(1+Psi(1)-r)) ;
	* o d :
	* 
	* for N from 1 to 28 do
	* 	r := 0 :
	* 	n := N-1 :
	*
	*	for l from iquo(n+3,2) to 70 do
	*		d := 0 :
	*		for s from 0 to n+1 do
	*		 d := d+(-1)^s*binomial(n+1,s)*binomial((s-1)/2,l) :
	*		od :
	*		if 2*l-n > 1 then
	*		r := r+d*(-1)^l*(Zeta(2*l-n) -1) :
	*		fi :
	*	od :
	*	print(evalf((-1)^n*2*r)) ;
	* od :
	* quit :
	*/
	private static double Kncoe[] = { .30459198558715155634315638246624251,
	.72037977439182833573548891941219706, -.12454959243861367729528855995001087,
	.27769457331927827002810119567456810e-1, -.67762371439822456447373550186163070e-2,
	.17238755142247705209823876688592170e-2, -.44817699064252933515310345718960928e-3,
	.11793660000155572716272710617753373e-3, -.31253894280980134452125172274246963e-4,
	.83173997012173283398932708991137488e-5, -.22191427643780045431149221890172210e-5,
	.59302266729329346291029599913617915e-6, -.15863051191470655433559920279603632e-6,
	.42459203983193603241777510648681429e-7, -.11369129616951114238848106591780146e-7,
	.304502217295931698401459168423403510e-8, -.81568455080753152802915013641723686e-9,
	.21852324749975455125936715817306383e-9, -.58546491441689515680751900276454407e-10,
	.15686348450871204869813586459513648e-10, -.42029496273143231373796179302482033e-11,
	.11261435719264907097227520956710754e-11, -.30174353636860279765375177200637590e-12,
	.80850955256389526647406571868193768e-13, -.21663779809421233144009565199997351e-13,
	.58047634271339391495076374966835526e-14, -.15553767189204733561108869588173845e-14,
	.41676108598040807753707828039353330e-15, -.11167065064221317094734023242188463e-15 };
	
	/** The digamma function in long double precision.
	* @param x the real value of the argument
	* @return the value of the digamma (psi) function at that point
	* @author Richard J. Mathar
	* @since 2005-11-24
	*/
	public static double digammal(double x){
		/* force into the interval 1..3 */
		if(x < 0.0){
			return digammal(1.0-x)+M_PIl/Math.tan(M_PIl*(1.0-x));	/* reflection formula */
		}else if(x < 1.0){
			return digammal(1.0+x)-1.0/x;
		}else if(x == 1.0){
			return -M_GAMMAl;
		}else if(x == 2.0){
			return 1.0-M_GAMMAl;
		}else if(x == 3.0){
			return 1.5-M_GAMMAl;
		}else if( x > 3.0){
			/* duplication formula */
			return 0.5*(digammal(x/2.0)+digammal((x+1.0)/2.0))+M_LN2l;
		}else{
			double Tn_1 = 1.0;		/* T_{n-1}(x), started at n=1 */
			double Tn = x - 2.0;	/* T_{n}(x)  , started at n=1 */
			double resul = Kncoe[0] + Kncoe[1] * Tn;

			x -= 2.0;

			for(int n = 2 ;n < Kncoe.length;n++){
				double Tn1 = 2.0 * x * Tn - Tn_1;	/* Chebyshev recursion, Eq. 22.7.4 Abramowitz-Stegun */
				resul += Kncoe[n]*Tn1 ;
				Tn_1 = Tn ;
				Tn = Tn1 ;
			}
			return resul ;
		}
	}
}
