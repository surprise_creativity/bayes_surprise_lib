/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: ObjectArtifact.java   Abstração de um artefato genérico.     */
/* --------------------------------------------------------------------------------- */

package surp.artifact;

import java.util.ArrayList;

/**
 * Classe que abstrai um artefato.
 * 
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *         
 */

public class ObjectArtifact {
	private ArrayList<Object> features;
	private ArrayList<Byte> featureType;
	
	/**
	 * Construtor do artefato.
	 */
	public ObjectArtifact(){
		features = new ArrayList<>();
		featureType = new ArrayList<>();
	}
	
	/**
	 * Adiciona um valor boolean na próxima característica do artefato.
	 * @param feature Valor booleano da característica.
	 */
	public void add(boolean feature){
		Boolean featureObj = feature;
		Byte featureTp = new Byte((byte)1);
		features.add(featureObj);
		featureType.add(featureTp);
	}
	
	/**
	 * Adiciona um valor inteiro (maior que 0) na próxima característica do artefato.
	 * @param feature Valor inteiro (maior que 0) da característica.
	 */
	public void add(int feature){
		Integer featureObj = feature;
		Byte featureTp = new Byte((byte)2);
		features.add(featureObj);
		featureType.add(featureTp);
	}
	
	/**
	 * Adiciona um valor real na próxima característica do artefato.
	 * @param feature Valor real da característica.
	 */
	public void add(double feature){
		Double featureObj = feature;
		Byte featureTp = new Byte((byte)3);
		features.add(featureObj);
		featureType.add(featureTp);
	}
	
	// Indica o Id do tipo da característica na posição index do artefato.
	// @param index Posição da característica no artefato.
	// @return O valor do Id do tipo da característica na posição index do artefato.
	private byte getIdType(int index){
		if(index >= 0 && index < featureType.size()){
			return featureType.get(index);
		}else{
			return 0;
		}
	}
	
	/**
	 * Indica se a característica da posição index do artefato é de tipo indefinido.
	 * @param index Posição da característica no artefato.
	 * @return True se a característica da posição index do artefato é de tipo indefinido.
	 */
	public boolean isUndefined(int index){
		return (getIdType(index) == 0);
	}
	
	/**
	 * Indica se a característica da posição index do artefato é de tipo booleano.
	 * @param index Posição da característica no artefato.
	 * @return True se a característica da posição index do artefato é de tipo booleano.
	 */
	public boolean isBoolean(int index){
		return (getIdType(index) == 1);
	}
	
	/**
	 * Indica se a característica da posição index do artefato é de tipo inteiro.
	 * @param index Posição da característica no artefato.
	 * @return True se a característica da posição index do artefato é de tipo inteiro.
	 */
	public boolean isInteger(int index){
		return (getIdType(index) == 2);
	}
	
	/**
	 * Indica se a característica da posição index do artefato é de tipo real.
	 * @param index Posição da característica no artefato.
	 * @return True se a característica da posição index do artefato é de tipo real.
	 */
	public boolean isDouble(int index){
		return (getIdType(index) == 3);
	}
	
	/**
	 * Lê, se possível (tipo compatível), o valor booleano da característica da posição index.
	 * @param index Posição da característica no artefato.
	 * @return Valor booleano da característica no artefato.
	 */
	public boolean getValueBoolean(int index){
		if(isBoolean(index)){
			Boolean featureObj = (Boolean)features.get(index);
			return featureObj;
		}else{
			return false;
		}
	}
	
	/**
	 * Modifica, se possível (tipo compatível), o valor booleano da característica da posição index.
	 * @param index Posição da característica no artefato.
	 * @param feature Novo valor booleano da característica no artefato. 
	 */
	public void setValueBoolean(int index, boolean feature){
		if(isBoolean(index)){
			Boolean featureObj = feature;
			features.set(index, featureObj);
		}
	}
	
	/**
	 * Lê, se possível (tipo compatível), o valor inteiro da característica da posição index.
	 * @param index Posição da característica no artefato.
	 * @return Valor inteiro da característica no artefato.
	 */
	public int getValueInteger(int index){
		if(isInteger(index)){
			Integer featureObj = (Integer)features.get(index);
			return featureObj;
		}else{
			return 0;
		}
	}
	
	/**
	 * Modifica, se possível (tipo compatível), o valor inteiro (maior que 0) da característica da posição index.
	 * @param index Posição da característica no artefato.
	 * @param feature Novo valor inteiro (maior que 0) da característica no artefato.
	 */
	public void setValueInteger(int index, int feature){
		if(isInteger(index)){
			Integer featureObj = feature;
			features.set(index, featureObj);
		}
	}
	
	/**
	 * Lê, se possível (tipo compatível), o valor real da característica da posição index.
	 * @param index Posição da característica no artefato.
	 * @return Valor real da característica no artefato.
	 */
	public double getValueDouble(int index){
		if(isDouble(index)){
			Double featureObj = (Double)features.get(index);
			return featureObj;
		}else{
			return 0.0;
		}
	}
	
	/**
	 * Modifica, se possível (tipo compatível), o valor real da característica da posição index.
	 * @param index Posição da característica no artefato.
	 * @param feature Novo valor real da característica no artefato.
	 */
	public void setValueDouble(int index, double feature){
		if(isDouble(index)){
			Double featureObj = feature;
			features.set(index, featureObj);
		}
	}
	
	/**
	 * Tamanho do artefato (em número de características).
	 * @return Número de características do artefato.
	 */
	public int size(){
		return features.size();
	}
	
	/**
	 * Deleta as características do artefato.
	 */
	public void clear(){
		features.clear();
		featureType.clear();
	}
	
	/**
	 * Gera uma cópia do artefato com toda as suas características.
	 * @return Cópia do artefato.
	 */
	public ObjectArtifact getCopy(){
		ObjectArtifact newObjArt = new ObjectArtifact();
		for(int index = 0;index < size();index++){
			byte typeFeature = getIdType(index);
			if(typeFeature == 1){
				boolean feature = getValueBoolean(index);
				newObjArt.add(feature);
			}else if(typeFeature == 2){
				int feature = getValueInteger(index);
				newObjArt.add(feature);
			}else if(typeFeature == 3){
				double feature = getValueDouble(index);
				newObjArt.add(feature);
			}
		}
		return newObjArt;
	}
	
	/**
	 * Copia os valores do artefato atual para o artefato destiny.
	 * @param destiny Artefato que irá receber uma cópia das características.
	 */
	public void copyTo(ObjectArtifact destiny){
		destiny.clear();
		for(int index = 0;index < size();index++){
			byte typeFeature = getIdType(index);
			if(typeFeature == 1){
				boolean feature = getValueBoolean(index);
				destiny.add(feature);
			}else if(typeFeature == 2){
				int feature = getValueInteger(index);
				destiny.add(feature);
			}else if(typeFeature == 3){
				double feature = getValueDouble(index);
				destiny.add(feature);
			}
		}
	}
}
