/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: ObjectModel.java   Abstração de um conjunto de Modelos.      */
/* --------------------------------------------------------------------------------- */

package surp.model;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Classe que abstrai um conjunto de Modelos de Surpresa Bayesiana.
 * 
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

public class ObjectModel {
	private ArrayList<TypeModel> featuresModel;
	
	/**
	 * Construtor.
	 */
	public ObjectModel(){
		featuresModel = new ArrayList<>();
	}
	
	/**
	 * Adiciona um Modelo Bayesiano para característica booleana.
	 * @param featureModel Modelo Bayesiano para dado booleano.
	 */
	public void add(BooleanModel featureModel){
		featuresModel.add(featureModel);
	}
	
	/**
	 * Adiciona um Modelo Bayesiano para característica inteira (maior que 0).
	 * @param featureModel Modelo Bayesiano para dado inteiro (maior que 0).
	 */
	public void add(IntegerModel featureModel){
		featuresModel.add(featureModel);
	}
	
	/**
	 * Adiciona um Modelo Bayesiano para característica real.
	 * @param featureModel Modelo Bayesiano para dado real.
	 */
	public void add(DoubleModel featureModel){
		featuresModel.add(featureModel);
	}
	
	// Indica o Id do tipo da característica na posição index dos modelos.
	// @param index Posição da característica nos modelos.
	// @return O valor do Id do tipo da característica na posição index dos modelos.
	private byte getIdType(int index){
		if(index >= 0 && index < featuresModel.size()){
			return featuresModel.get(index).getIdType();
		}else{
			return 0;
		}
	}
	
	/**
	 * Indica se a característica da posição index dos modelos é de tipo indefinido.
	 * @param index Posição da característica nos modelos.
	 * @return True se a característica da posição index dos modelos é de tipo indefinido.
	 */
	public boolean isUndefined(int index){
		return (getIdType(index) == 0);
	}
	
	/**
	 * Indica se a característica da posição index dos modelos é de tipo booleano.
	 * @param index Posição da característica nos modelos.
	 * @return True se a característica da posição index dos modelos é de tipo booleano.
	 */
	public boolean isBoolean(int index){
		return (getIdType(index) == 1);
	}
	
	/**
	 * Indica se a característica da posição index dos modelos é de tipo inteiro (maior que 0).
	 * @param index Posição da característica nos modelos.
	 * @return True se a característica da posição index dos modelos é de tipo inteiro (maior que 0).
	 */
	public boolean isInteger(int index){
		return (getIdType(index) == 2);
	}
	
	/**
	 * Indica se a característica da posição index dos modelos é de tipo real.
	 * @param index Posição da característica nos modelos.
	 * @return True se a característica da posição index dos modelos é de tipo real.
	 */
	public boolean isDouble(int index){
		return (getIdType(index) == 3);
	}
	
	/**
	 * Calcula, se possível (tipo compatível), o valor da surpresa de um valor booleano da característica da posição index.
	 * @param index Posição da característica nos modelos.
	 * @param value Valor booleano sobre o qual será medida a surpresa da característica.
	 * @return Valor de surpresa.
	 */
	public double getSurpriseBoolean(int index, boolean value){
		if(isBoolean(index)){
			BooleanModel featureModel = (BooleanModel)featuresModel.get(index);
			return (featureModel.getSurprise(value));
		}else{
			return 0.0;
		}
	}
	
	/**
	 * Apreende, se possível (tipo compatível), o valor booleano da característica da posição index.
	 * @param index Posição da característica nos modelos.
	 * @param value Valor booleano que será apreendido.
	 */
	public void learnBoolean(int index, boolean value){
		if(isBoolean(index)){
			BooleanModel featureModel = (BooleanModel)featuresModel.get(index);
			featureModel.learn(value);
		}
	}
	
	/**
	 * Calcula, se possível (tipo compatível), o valor da surpresa de um valor inteiro (maior que 0) da característica da posição index.
	 * @param index Posição da característica nos modelos.
	 * @param value Valor inteiro (maior que 0) sobre o qual será medida a surpresa da característica.
	 * @return Valor de surpresa.
	 */
	public double getSurpriseInteger(int index, int value){
		if(isInteger(index)){
			IntegerModel featureModel = (IntegerModel)featuresModel.get(index);
			return (featureModel.getSurprise(value));
		}else{
			return 0.0;
		}
	}
	
	/**
	 * Apreende, se possível (tipo compatível), o valor inteiro (maior que 0) da característica da posição index.
	 * @param index Posição da característica nos modelos.
	 * @param value Valor inteiro (maior que 0) que será apreendido.
	 */
	public void learnInteger(int index, int value){
		if(isInteger(index)){
			IntegerModel featureModel = (IntegerModel)featuresModel.get(index);
			featureModel.learn(value);
		}
	}
	
	/**
	 * Calcula, se possível (tipo compatível), o valor da surpresa de um valor real da característica da posição index.
	 * @param index Posição da característica nos modelos.
	 * @param value Valor real sobre o qual será medida a surpresa da característica.
	 * @return Valor de surpresa.
	 */
	public double getSurpriseDouble(int index, double value){
		if(isDouble(index)){
			DoubleModel featureModel = (DoubleModel)featuresModel.get(index);
			return (featureModel.getSurprise(value));
		}else{
			return 0.0;
		}
	}
	
	/**
	 * Apreende, se possível (tipo compatível), o valor real da característica da posição index.
	 * @param index Posição da característica nos modelos.
	 * @param value Valor real que será apreendido.
	 */
	public void learnDouble(int index, double value){
		if(isDouble(index)){
			DoubleModel featureModel = (DoubleModel)featuresModel.get(index);
			featureModel.learn(value);
		}
	}
	
	/**
	 * Escreve os parâmetros de todos os modelos bayesianos do conjunto em uma saída buferrizada (ex: arquivo).
	 * @param bos Saída bufferizada.
	 * @return True se foi possível escrever, false em caso contrário.
	 */
	public boolean write(BufferedOutputStream bos){
		boolean result = true;
		for(int index = 0;(index < featuresModel.size()) && result;index++){
			TypeModel featureModel = featuresModel.get(index);
			result = featureModel.write(bos);
		}
		return result;
	}
	
	/**
	 * Escreve os parâmetros de todos os modelos bayesianos do conjunto em um arquivo binário.
	 * @param fileName Endereço do arquivo binário a ser escrito (se existir, sobreescreve).
	 * @return True se foi possível escrever, false em caso contrário.
	 */
	public boolean write(String fileName){
		boolean result = true;
		File fp = new File(fileName);
		try(FileOutputStream fos = new FileOutputStream(fp)){
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			result = write(bos);
			bos.close();
			fos.close();
		}catch(IOException ioe){
			result = false;
	    }
		return result;
	}
	
	/**
	 * Lê os parâmetros de todos os modelos bayesianos do conjunto de uma entrada buferrizada (ex: arquivo).
	 * @param bis Entrada bufferizada.
	 * @return True se foi possível ler, false em caso contrário.
	 */
	public boolean read(BufferedInputStream bis){
		boolean result = true;
		for(int index = 0;(index < featuresModel.size()) && result;index++){
			TypeModel featureModel = featuresModel.get(index);
			result = featureModel.read(bis);
		}
		return result;
	}
	
	/**
	 * Lê os parâmetros de todos os modelos bayesianos do conjunto de um arquivo binário.
	 * @param fileName Endereço do arquivo binário a ser lido.
	 * @return True se foi possível ler, false em caso contrário.
	 */
	public boolean read(String fileName){
		boolean result = true;
		File fp = new File(fileName);
		try(FileInputStream fis = new FileInputStream(fp)){
			BufferedInputStream bis = new BufferedInputStream(fis);
			result = read(bis);
			bis.close();
			fis.close();
		}catch(IOException ioe){
			result = false;
	    }
		return result;
	}
	
	/**
	 * Tamanho do conjunto de Modelos Bayesianos (em número de modelos/características).
	 * @return Número de Modelos Bayesianos no conjunto.
	 */
	public int size(){
		return featuresModel.size();
	}
	
	/**
	 * Deleta os Modelos Bayesianos do conjunto.
	 */
	public void clear(){
		featuresModel.clear();
	}
	
	/**
	 * Gera uma cópia do conjunto de modelos com a referência a toda os seus Modelos Bayesianos.
	 * @return Cópia do conjunto de modelos.
	 */
	public ObjectModel getCopy(){
		ObjectModel newObjMdl = new ObjectModel();
		for(int index = 0;index < size();index++){
			byte typeFeature = getIdType(index);
			if(typeFeature == 1){
				BooleanModel featureModel = (BooleanModel)featuresModel.get(index);
				newObjMdl.add(featureModel);
			}else if(typeFeature == 2){
				IntegerModel featureModel = (IntegerModel)featuresModel.get(index);
				newObjMdl.add(featureModel);
			}else if(typeFeature == 3){
				DoubleModel featureModel = (DoubleModel)featuresModel.get(index);
				newObjMdl.add(featureModel);
			}
		}
		return newObjMdl;
	}
	
	/**
	 * Copia as referências do conjunto de modelos atual para o conjunto de modelos destiny.
	 * @param destiny Conjunto de modelos que irá receber uma cópia das referências dos Modelos Bayesianos.
	 */
	public void copyTo(ObjectModel destiny){
		destiny.clear();
		for(int index = 0;index < size();index++){
			byte typeFeature = getIdType(index);
			if(typeFeature == 1){
				BooleanModel featureModel = (BooleanModel)featuresModel.get(index);
				destiny.add(featureModel);
			}else if(typeFeature == 2){
				IntegerModel featureModel = (IntegerModel)featuresModel.get(index);
				destiny.add(featureModel);
			}else if(typeFeature == 3){
				DoubleModel featureModel = (DoubleModel)featuresModel.get(index);
				destiny.add(featureModel);
			}
		}
	}
}
