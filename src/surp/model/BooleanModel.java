/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: BooleanModel.java   M. Bayesiano genérico para tipo boleano. */
/* --------------------------------------------------------------------------------- */

package surp.model;

import java.util.ArrayList;

/**
 * Modelo Bayesiano para calculo de surpresa em tipo de dados booleano.
 * 
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *         
 */

public interface BooleanModel extends TypeModel{
	/**
	 * Apreende um conjunto de dados com amount_total booleanos, onde amount_true destes booleanos tem valor 'true'.
	 * @param amount_true Quantidade de booleanos true.
	 * @param amount_total Quantidade total de booleanos (true e false).
	 */
	public void learn(int amount_true, int amount_total);
	
	/**
	 * Apreende um conjunto de dados booleano passado via lista.
	 * @param values Lista de booleanos.
	 */
	public void learn(ArrayList<Boolean> values);
	
	/**
	 * Apreende um valor booleano. 
	 * @param value Valor booleano.
	 */
	public void learn(boolean value);
	
	/**
	 * Calcula a surpresa de um conjunto de dados com amount_total booleanos, onde amount_true destes booleanos tem valor 'true'.
	 * @param amount_true Quantidade de booleanos true.
	 * @param amount_total Quantidade total de booleanos (true e false).
	 * @return Valor de surpresa.
	 */
	public double getSurprise(int amount_true, int amount_total);
	
	/**
	 * Calcula a surpresa de um conjunto de dados booleano passado via lista.
	 * @param values Lista de booleanos.
	 * @return Valor de surpresa.
	 */
	public double getSurprise(ArrayList<Boolean> values);
	
	/**
	 * Calcula a surpresa de um valor booleano.
	 * @param value Valor booleano.
	 * @return Valor de surpresa.
	 */
	public double getSurprise(boolean value);
}
