/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: GaussianDistribution.java   Modelo para dado real(var fixa). */
/* --------------------------------------------------------------------------------- */

package surp.model.distribution;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import surp.model.DoubleModel;

/**
 * Classe que implementa um Modelo Bayesiano para dados reais (apenas variância fixa) com distribuição conjugada a priori Gaussiana.
 * Baseado na seção 3.3 do artigo "Of bits and wows: A Bayesian theory of surprise with applications to attention"
 * de Baldi, P. e Itti, L (2010).
 * 
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

public class GaussianDistribution implements DoubleModel {
	/**
	 * Parâmetros da distribuição (priori):
	 * média da distribuição gaussiana que modela o parâmetro de média das amostras de dados.
	 */
	private double mean1;
	
	/**
	 * Parâmetros da distribuição (priori):
	 * variância da distribuição gaussiana que modela o parâmetro de média das amostras de dados.
	 */
	private double variance1;
	
	/**
	 * Parâmetros da distribuição (likehood):
	 * variância (fixa) das amostras de dados.
	 */
	private double variance_data;
	
	/**
	 * Construtor padrão (variância fixa em 1.0).
	 */
	public GaussianDistribution() {
		mean1 = 0.0;
		variance1 = 1.0;
		variance_data = 1.0;
	}
	
	/**
	 * Construtor para um valor de variância fixa passado.
	 * @param _variance_data Variância fixa das amostras de dados.
	 */
	public GaussianDistribution(double _variance_data) {
		if(_variance_data < 0.1){
			_variance_data = 0.1;
		}
		mean1 = 0.0;
		variance1 = 1.0;
		variance_data = _variance_data;
	}
	
	/**
	 * Construtor para valor de variância fixa passado e parâmetros de uma gaussiana para a média.
	 * @param _mean1 Média da distribuição gaussiana que modela o parâmetro de média das amostras de dados.
	 * @param _variance1 Variância da distribuição gaussiana que modela o parâmetro de média das amostras de dados.
	 * @param _variance_data Variância (fixa) das amostras de dados.
	 */
	public GaussianDistribution(double _mean1, double _variance1, double _variance_data) {
		if(_variance1 < 0.1){
			_variance1 = 0.1;
		}
		if(_variance_data < 0.1){
			_variance_data = 0.1;
		}
		mean1 = _mean1;
		variance1 = _variance1;
		variance_data = _variance_data;
	}
	
	@Override
	public void learn(double mean, double variance, int amount_total) {
		double inv_variance = ((1.0 / variance1) + (((double)amount_total) / variance_data));
		mean1 = ((mean1 / variance1) + ((amount_total * mean) / variance_data)) / inv_variance;
		variance1 = (1.0 / inv_variance);
	}

	@Override
	public void learn(ArrayList<Double> values) {
		double mean = 0.0;
		int amount_total = values.size();
		for(int i = 0;i < amount_total;i++){
			double value = values.get(i);
			mean += value;
		}
		if(amount_total > 0){
			mean = mean / ((double)amount_total);
		}
		learn(mean, variance_data, amount_total);
	}

	@Override
	public void learn(double value) {
		double mean = value;
		int amount_total = 1;
		learn(mean, variance_data, amount_total);
	}
	
	// Função que calcula surpresa bayesiana usando a fórmula exata.
	// @param mean Média amostral do conjunto de valores reais.
	// @param variance Variância amostral (porém, com denominador igual a amount_total) do conjunto de valores reais.
	// @param amount_total Quantidade total de valores reais.
	// @return Valor de surpresa.
	private double surprise(double mean, double variance, int amount_total){
		double result = 0.0;
		
		double sum_variance = (variance_data + amount_total * variance1);
		
		double const_variance = Math.sqrt(variance_data / sum_variance);
		
		double deaviation = (mean1 - mean);
		deaviation = deaviation * deaviation;
		
		double variance_relative = (amount_total * variance1) / (2.0 * variance_data);
		
		result = Math.log(const_variance) + variance_relative
				+ ((amount_total * variance_relative) * (deaviation / sum_variance));
		
		return result;
	}
	
	// Função que calcula surpresa bayesiana usando a fórmula aproximada.
	// @param mean Média amostral do conjunto de valores reais.
	// @param variance Variância amostral (porém, com denominador igual a amount_total) do conjunto de valores reais.
	// @param amount_total Quantidade total de valores reais.
	// @return Valor de surpresa.
	private double surpriseApproximation(double mean, double variance, int amount_total){
		double result = 0.0;
		
		double deaviation = (mean1 - mean);
		deaviation = deaviation * deaviation;
		
		result = ((((double)amount_total) / (2.0 * variance_data)) * (variance1 + deaviation));
		
		return result;
	}

	@Override
	public double getSurprise(double mean, double variance, int amount_total) {
		double result = 0.0;
		if(amount_total >= 100){
			result = surpriseApproximation(mean, variance, amount_total);
		}else{
			result = surprise(mean, variance, amount_total);
		}
		return result;
	}

	@Override
	public double getSurprise(ArrayList<Double> values) {
		double mean = 0.0;
		int amount_total = values.size();
		for(int i = 0;i < amount_total;i++){
			double value = values.get(i);
			mean += value;
		}
		if(amount_total > 0){
			mean = mean / ((double)amount_total);
		}
		return getSurprise(mean, variance_data, amount_total);
	}

	@Override
	public double getSurprise(double value) {
		double mean = value;
		int amount_total = 1;
		return getSurprise(mean, variance_data, amount_total);
	}

	@Override
	public byte getIdType() {
		return 3;
	}

	@Override
	public boolean write(BufferedOutputStream bos) {
		boolean result = true;
		
		byte [] param_bytes = new byte[3 * Double.BYTES];
		ByteBuffer.wrap(param_bytes, 0, Double.BYTES).putDouble(mean1);
		ByteBuffer.wrap(param_bytes, Double.BYTES, Double.BYTES).putDouble(variance1);
		ByteBuffer.wrap(param_bytes, (2 * Double.BYTES), Double.BYTES).putDouble(variance_data);
		
		try{
			bos.write(param_bytes);
			bos.flush();
		}catch(IOException ioe){
			result = false;
		}
		
		return result;
	}

	@Override
	public boolean read(BufferedInputStream bis) {
		boolean result = true;
		
		byte [] param_bytes = new byte[3 * Double.BYTES];
		
		try{
			if(bis.read(param_bytes) != (3 * Double.BYTES)){
				result = false;
			}
		}catch(IOException ioe){
			result = false;
		}
		
		if(result){
			mean1 = ByteBuffer.wrap(param_bytes, 0, Double.BYTES).getDouble();
			variance1 = ByteBuffer.wrap(param_bytes, Double.BYTES, Double.BYTES).getDouble();
			variance_data = ByteBuffer.wrap(param_bytes, (2 * Double.BYTES), Double.BYTES).getDouble();
		}
		
		return result;
	}

}
