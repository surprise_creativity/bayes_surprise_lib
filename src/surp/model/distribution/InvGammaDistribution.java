/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*               File: InvGammaDistribution.java   Modelo para dado real(mean fixa). */
/* --------------------------------------------------------------------------------- */

package surp.model.distribution;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import cern.jet.stat.Gamma;
import surp.Digamma;
import surp.model.DoubleModel;

/**
 * Classe que implementa um Modelo Bayesiano para dados reais (apenas média fixa) com distribuição conjugada a priori Inversa Gamma Escalada.
 * Baseado na seção 3.4 do artigo "Of bits and wows: A Bayesian theory of surprise with applications to attention"
 * de Baldi, P. e Itti, L (2010).
 * 
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

public class InvGammaDistribution implements DoubleModel {
	/**
	 * Parâmetros da distribuição (priori):
	 * primeiro parâmetro da distribuição inversa gamma escalada que modela o parâmetro de variância das amostras de dados.
	 */
	private double v1;
	
	/**
	 * Parâmetros da distribuição (priori):
	 * segundo parâmetro da distribuição inversa gamma escalada que modela o parâmetro de variância das amostras de dados.
	 */
	private double s1;
	
	/**
	 * Parâmetros da distribuição (likehood):
	 * média (fixa) das amostras de dados.
	 */
	private double mean_data;
	
	/**
	 * Construtor padrão.
	 */
	public InvGammaDistribution() {
		v1 = 1;
		s1 = 1;
		mean_data = 0.0;
	}
	
	/**
	 * Construtor para um valor de média fixa passado.
	 * @param _mean_data Média (fixa) das amostras de dados.
	 */
	public InvGammaDistribution(double _mean_data) {
		v1 = 1;
		s1 = 1;
		mean_data = _mean_data;
	}
	
	/**
	 * Construtor para valores de parâmetros a priori passados.
	 * @param _v1 Primeiro parâmetro da distribuição inversa gamma escalada que modela o parâmetro de variância das amostras de dados.
	 * @param _s1 Segundo parâmetro da distribuição inversa gamma escalada que modela o parâmetro de variância das amostras de dados.
	 * @param _mean_data Média (fixa) das amostras de dados.
	 */
	public InvGammaDistribution(double _v1, double _s1, double _mean_data) {
		if(_v1 < 0.1){
			_v1 = 0.1;
		}
		if(_s1 < 0.1){
			_s1 = 0.1;
		}
		v1 = _v1;
		s1 = _s1;
		mean_data = _mean_data;
	}

	@Override
	public void learn(double mean, double variance, int amount_total) {
		double v2 = (v1 + amount_total);
		s1 = ((v1 * s1 + amount_total * variance) / v2);
		v1 = v2;
	}

	@Override
	public void learn(ArrayList<Double> values) {
		double variance = 0.0;
		int amount_total = values.size();
		for(int i = 0;i < amount_total;i++){
			double value = values.get(i);
			value = (value - mean_data);
			variance += (value * value);
		}
		if(amount_total > 0){
			variance = variance / ((double)amount_total);
		}
		learn(mean_data, variance, amount_total);
	}

	@Override
	public void learn(double value) {
		double variance = (value - mean_data);
		variance = variance * variance;
		int amount_total = 1;
		learn(mean_data, variance, amount_total);
	}
	
	// Função que calcula surpresa bayesiana usando a fórmula exata.
	// @param mean Média amostral do conjunto de valores reais.
	// @param variance Variância amostral (porém, com denominador igual a amount_total) do conjunto de valores reais.
	// @param amount_total Quantidade total de valores reais.
	// @return Valor de surpresa.
	private double surprise(double mean, double variance, int amount_total){
		double result = 0.0;
		
		double v2 = (v1 + amount_total);
		double s2 = ((v1 * s1 + amount_total * variance) / v2);
		
		double v1_half = (v1 / 2.0);
		double v2_half = (v2 / 2.0);
		
		double const_pt1 = Math.pow(v1_half, v1_half) / Math.pow(v2_half, v2_half);
		double const_pt2 = Math.pow(s1, v1_half) / Math.pow(s2, v2_half);
		double const_pt3 = Gamma.gamma(v2_half) / Gamma.gamma(v1_half);
		double const_inv_gama = const_pt1 * const_pt2 * const_pt3;
		
		result = Math.log(const_inv_gama) + (((double)amount_total) / 2.0) * ((variance / s1) - (Digamma.digammal(v1_half) + Math.log((2.0 / (v1 * s1)))));
		
		return result;
	}
	
	// Função que calcula surpresa bayesiana usando a fórmula aproximada.
	// @param mean Média amostral do conjunto de valores reais.
	// @param variance Variância amostral (porém, com denominador igual a amount_total) do conjunto de valores reais.
	// @param amount_total Quantidade total de valores reais.
	// @return Valor de surpresa.
	private double surpriseApproximation(double mean, double variance, int amount_total) {
		double result = 0.0;
		
		result = ((((double)amount_total) / 2.0) * ((variance / s1) + Math.log((v1 * s1) / (2.0 * variance)) - Digamma.digammal(v1 / 2.0)));
		
		return result;
	}

	@Override
	public double getSurprise(double mean, double variance, int amount_total) {
		double result = 0.0;
		if((amount_total >= 100) && (variance > 0)){
			result = surpriseApproximation(mean, variance, amount_total);
		}else{
			result = surprise(mean, variance, amount_total);
		}
		return result;
	}

	@Override
	public double getSurprise(ArrayList<Double> values) {
		double variance = 0.0;
		int amount_total = values.size();
		for(int i = 0;i < amount_total;i++){
			double value = values.get(i);
			value = (value - mean_data);
			variance += (value * value);
		}
		if(amount_total > 0){
			variance = variance / ((double)amount_total);
		}
		return getSurprise(mean_data, variance, amount_total);
	}

	@Override
	public double getSurprise(double value) {
		double variance = (value - mean_data);
		variance = variance * variance;
		int amount_total = 1;
		return getSurprise(mean_data, variance, amount_total);
	}
	
	@Override
	public byte getIdType() {
		return 3;
	}

	@Override
	public boolean write(BufferedOutputStream bos) {
		boolean result = true;
		
		byte [] param_bytes = new byte[3 * Double.BYTES];
		ByteBuffer.wrap(param_bytes, 0, Double.BYTES).putDouble(v1);
		ByteBuffer.wrap(param_bytes, Double.BYTES, Double.BYTES).putDouble(s1);
		ByteBuffer.wrap(param_bytes, (2 * Double.BYTES), Double.BYTES).putDouble(mean_data);
		
		try{
			bos.write(param_bytes);
			bos.flush();
		}catch(IOException ioe){
			result = false;
		}
		
		return result;
	}

	@Override
	public boolean read(BufferedInputStream bis) {
		boolean result = true;
		
		byte [] param_bytes = new byte[3 * Double.BYTES];
		
		try{
			if(bis.read(param_bytes) != (3 * Double.BYTES)){
				result = false;
			}
		}catch(IOException ioe){
			result = false;
		}
		
		if(result){
			v1 = ByteBuffer.wrap(param_bytes, 0, Double.BYTES).getDouble();
			s1 = ByteBuffer.wrap(param_bytes, Double.BYTES, Double.BYTES).getDouble();
			mean_data = ByteBuffer.wrap(param_bytes, (2 * Double.BYTES), Double.BYTES).getDouble();
		}
		
		return result;
	}

}
