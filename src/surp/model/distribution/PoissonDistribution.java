/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: PoissonDistribution.java   Modelo para dado inteiro(>0).     */
/* --------------------------------------------------------------------------------- */

package surp.model.distribution;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import cern.jet.stat.Gamma;
//import cern.jet.stat.Gamma;
import surp.Digamma;
import surp.model.IntegerModel;

/**
 * Classe que implementa um Modelo Bayesiano para dados inteiros (maiores que 0) com distribuição conjugada a priori Poisson.
 * Baseado na seção 3.2 do artigo "Of bits and wows: A Bayesian theory of surprise with applications to attention"
 * de Baldi, P. e Itti, L (2010).
 * 
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

public class PoissonDistribution implements IntegerModel {
	/**
	 * Parâmetros da distribuição (priori):
	 * primeiro parâmetro da distribuição Poisson.
	 */
	private double a1;
	
	/**
	 * Parâmetros da distribuição (priori):
	 * segundo parâmetro da distribuição Poisson.
	 */
	private double b1;
	
	/**
	 * Construtor padrão.
	 */
	public PoissonDistribution(){
		a1 = 1;
		b1 = 1;
	}
	
	/**
	 * Construtor para um valor de média a priori.
	 * @param mean_priori Valor de média a priori.
	 */
	public PoissonDistribution(double mean_priori){
		if(mean_priori < 0){
			mean_priori = 0;
		}
		if(mean_priori > 0){
			b1 = 1;
			a1 = mean_priori;
		}else if(mean_priori == 0){
			b1 = 1000000;
			a1 = 1;
		}
	}
	
	/**
	 * Construtor para valores de parâmetros a priori passados.
	 * @param _a1 Primeiro parâmetro da distribuição Poisson.
	 * @param _b1 Segundo parâmetro da distribuição Poisson.
	 */
	public PoissonDistribution(double _a1, double _b1){
		if(_a1 < 1){
			_a1 = 1;
		}
		if(_b1 < 1){
			_b1 = 1;
		}
		a1 = _a1;
		b1 = _b1;
	}
		
	@Override
	public void learn(double mean, int amount_total) {
		a1 = a1 + amount_total * mean;
		b1 = b1 + amount_total;
	}

	@Override
	public void learn(ArrayList<Integer> values) {
		double mean = 0.0;
		int amount_total = values.size();
		for(int i = 0;i < amount_total;i++){
			int value = values.get(i);
			if(value > 0){
				mean += value;
			}
		}
		if(amount_total > 0){
			mean = mean / ((double)amount_total);
		}
		learn(mean, amount_total);
	}

	@Override
	public void learn(int value) {
		double mean = 0.0;
		int amount_total = 1;
		if(value > 0){
			mean += value;
		}
		learn(mean, amount_total);
	}

	// Função que calcula surpresa bayesiana usando a fórmula exata.
	// @param mean Média amostral do conjunto de números inteiros (maiores que 0).
	// @param amount_total Quantidade total de números inteiros (maiores que 0).
	// @return Valor de surpresa.
	private double surprise(double mean, int amount_total){
		double result = 0.0;
		
		double b2 = (b1 + amount_total);
 		double const_gamma = Gamma.gamma(a1 + amount_total * mean) / Gamma.gamma(a1); 
		result = a1 * Math.log(b1 / b2)
				 + Math.log(const_gamma)
				 + amount_total * ((a1 / b1) + mean * (Math.log(b1) - Digamma.digammal(a1) - Math.log(b2)));
		
		return result;
	}
	
	// Função que calcula surpresa bayesiana usando a fórmula aproximada.
	// @param mean Média amostral do conjunto de números inteiros (maiores que 0).
	// @param amount_total Quantidade total de números inteiros (maiores que 0).
	// @return Valor de surpresa.
	private double surpriseApproximation(double mean, int amount_total){
		double result = 0.0;
		
		result = amount_total * ((a1 / b1) - mean * (1.0 - Math.log(mean) + Digamma.digammal(a1) - Math.log(b1)));
		
		return result;
	}
	
	@Override
	public double getSurprise(double mean, int amount_total) {
		double result = 0.0;
		
		if(amount_total > 0){
			// Versão aproximada:
			result = surpriseApproximation(mean, amount_total);
		}else{
			// Versão exata (Problemática para qualquer quantidade razoável de dados por causa da const_gamma):
			result = surprise(mean, amount_total);
		}
		
		return result;
	}

	@Override
	public double getSurprise(ArrayList<Integer> values) {
		double mean = 0.0;
		int amount_total = values.size();
		for(int i = 0;i < amount_total;i++){
			int value = values.get(i);
			if(value > 0){
				mean += value;
			}
		}
		if(amount_total > 0){
			mean = mean / ((double)amount_total);
		}
		return getSurprise(mean, amount_total);
	}

	@Override
	public double getSurprise(int value) {
		double mean = 0.0;
		int amount_total = 1;
		if(value > 0){
			mean += value;
		}
		return getSurprise(mean, amount_total);
	}

	@Override
	public byte getIdType() {
		return 2;
	}

	@Override
	public boolean write(BufferedOutputStream bos) {
		boolean result = true;
		
		byte [] param_bytes = new byte[2 * Double.BYTES];
		ByteBuffer.wrap(param_bytes, 0, Double.BYTES).putDouble(a1);
		ByteBuffer.wrap(param_bytes, Double.BYTES, Double.BYTES).putDouble(b1);
		
		try{
			bos.write(param_bytes);
			bos.flush();
		}catch(IOException ioe){
			result = false;
		}
		
		return result;
	}

	@Override
	public boolean read(BufferedInputStream bis) {
		boolean result = true;
		
		byte [] param_bytes = new byte[2 * Double.BYTES];
		
		try{
			if(bis.read(param_bytes) != (2 * Double.BYTES)){
				result = false;
			}
		}catch(IOException ioe){
			result = false;
		}
		
		if(result){
			a1 = ByteBuffer.wrap(param_bytes, 0, Double.BYTES).getDouble();
			b1 = ByteBuffer.wrap(param_bytes, Double.BYTES, Double.BYTES).getDouble();
		}
		
		return result;
	}
	
}
