/* --------------------------------------------------------------------------------- */
/*                               PUC-MG, Unidade COREL.                              */
/*                Departamento de Pós-Graduação em Engenharia Elétrica               */
/*                 2016-2017 Coração Eucarístico, Belo Horizonte - MG                */
/*                              http://www.pucminas.br/                              */
/*                                                                                   */
/*                         Author: Willian Antônio dos Santos                        */
/*                              Will1Dexter@hotmail.com                              */
/*                                                                                   */
/*               **     This banner notice must not be removed      **               */
/* --------------------------------------------------------------------------------- */
/*  Copyright(c) 2016, Willian Antônio dos Santos                                    */
/*  All rights reserved.                                                             */
/*                                                                                   */
/*  Redistribution and use in source and binary forms, with or without               */
/*  modification, are permitted provided that the following conditions are met :     */
/*                                                                                   */
/*  1. Redistributions of source code must retain the above copyright notice, this   */
/*     list of conditions and the following disclaimer.                              */
/*  2. Redistributions in binary form must reproduce the above copyright notice,     */
/*     this list of conditions and the following disclaimer in the documentation     */
/*     and / or other materials provided with the distribution.                      */
/*                                                                                   */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  */
/*  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    */
/*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           */
/*  DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR   */
/*  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   */
/*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     */
/*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      */
/*  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    */
/*  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     */
/*                                                                                   */
/*  The views and conclusions contained in the software and documentation are those  */
/*  of the authors and should not be interpreted as representing official policies,  */
/*  either expressed or implied, of the FreeBSD Project.                             */
/* --------------------------------------------------------------------------------- */
/*                File: Exemplo.java   Exemplos de uso da lib 'surp' de Surpresa     */
/*                                      Bayesiana.                                   */
/* --------------------------------------------------------------------------------- */

import surp.artifact.ObjectArtifact;
import surp.evaluation.ArtifactEval;
import surp.evaluation.SimpleIntegrater;
import surp.model.ObjectModel;
import surp.model.distribution.BetaDistribution;
import surp.model.distribution.GaussianDistribution;
import surp.model.distribution.GssInvGammaDistribution;
import surp.model.distribution.InvGammaDistribution;
import surp.model.distribution.PoissonDistribution;

/**
 * Exemplo de uso da biblioteca de surpresa bayesiana.
 * 
 * @author Willian Antônio dos Santos (Will1Dexter@hotmail.com),
 *         pesquisador do departamento de Mestrado em Engenharia Elétrica da
 *         Pontifícia Universidade Católica de Minas Gerais - Brasil.
 *
 */

public class Exemplo {

	public static void main(String[] args) {
		// Criação dos artefatos:
		ObjectArtifact objArt1 = new ObjectArtifact();
		objArt1.add(true);
		objArt1.add(2);   // > 0
		objArt1.add(2.0); // var  = 2
		objArt1.add(5.0); // mean = 3
		objArt1.add(-2.0);
		
		ObjectArtifact objArt2 = new ObjectArtifact();
		objArt2.add(true);
		objArt2.add(3);   // > 0
		objArt2.add(1.0); // var  = 2
		objArt2.add(2.0); // mean = 3
		objArt2.add(-1.0);
		
		ObjectArtifact objArt3 = new ObjectArtifact();
		objArt3.add(false);
		objArt3.add(1);   // > 0
		objArt3.add(2.5); // var  = 2
		objArt3.add(6.0); // mean = 3
		objArt3.add(8.0);
		
		ObjectArtifact objArt4 = new ObjectArtifact();
		objArt4.add(false);
		objArt4.add(5);   // > 0
		objArt4.add(2.0); // var  = 2
		objArt4.add(3.0); // mean = 3
		objArt4.add(-5.0);
		
		ObjectArtifact objArt5 = new ObjectArtifact();
		objArt5.add(true);
		objArt5.add(6);    // > 0
		objArt5.add(-0.5); // var  = 2
		objArt5.add(4.0);  // mean = 3
		objArt5.add(10.0);
		
		// Criação do objeto para conjunto de modelos bayesianos:
		ObjectModel objMdl = new ObjectModel();
		
		// Criação de modelos bayesianos para cada tipo de distribuição:
		BetaDistribution boolDist = new BetaDistribution(0.5);                   // Dado booleano. Neste exemplo com média a priori de 0.5 (igual uma moeda não viciada).
		PoissonDistribution intDist = new PoissonDistribution(2.0);              // Dado inteiro (maior que 0). Neste exemplo com média a priori de 2.0.
		GaussianDistribution doubleDistFixVar = new GaussianDistribution(2.0);   // Dado real com apenas a variancia fixa em 2.
		InvGammaDistribution doubleDistFixMean = new InvGammaDistribution(3.0);  // Dado real com apenas a média fixa em 3.
		GssInvGammaDistribution doubleDistNoFix = new GssInvGammaDistribution(); // Dado real sem média ou variância fixa.
		
		// Adição dos modelos bayesianos ao objeto de conjunto de modelos: 
		objMdl.add(boolDist);
		objMdl.add(intDist);           // > 0
		objMdl.add(doubleDistFixVar);  // var  = 2
		objMdl.add(doubleDistFixMean); // mean = 3
		objMdl.add(doubleDistNoFix);
		
		// Obs: a ordem dos tipos de dados deve ser a mesma dos artefatos.
		
		// Agregador das surpresas das características do artefato:
		SimpleIntegrater itg = new SimpleIntegrater(); // Apenas soma a surpresa.
		
		// Utilizando o método estático isCompatible da classe ArtifactEval, avalia compatibilidade dos tipos entre o conjunto de modelos e os artefatos:
		if(ArtifactEval.isCompatible(objMdl, objArt1) && ArtifactEval.isCompatible(objMdl, objArt2) && ArtifactEval.isCompatible(objMdl, objArt3)  && ArtifactEval.isCompatible(objMdl, objArt4)  && ArtifactEval.isCompatible(objMdl, objArt5)){
			System.out.println("\n\nFormato compatível do modelo em relação aos artefatos!\n\n");
		}
		
		System.out.println("----------------------------------------------------------------\n\n");
		
		// Gravação dos parâmetros dos modelos bayesianos no conjunto de modelos:
		
		System.out.println("Gravando modelos iniciais!\n\n");
		
		objMdl.write("modelos.dat");
		
		// Obs: a escrita e leitura correta (em termos de órdem de modelos bayesianos) é responsabilidade do usuário da biblioteca.
		//      Isto é garantido pela utilização de conjunto de modelos com mesmas distribuições entre escrita e leitura.
		
		// Testes de avaliação de surpresa e treinamento:
		
		System.out.println("----------------------------------------------------------------\n\n");
		
		System.out.println("Apenas modelos a priori (i.e. sem apreender nenhum artefato)!\n\n");
		
		System.out.println("Surpresa do artefato 1: " + ArtifactEval.getSurprise(objMdl, objArt1, itg) + "\n");
		
		System.out.println("Surpresa do artefato 2: " + ArtifactEval.getSurprise(objMdl, objArt2, itg) + "\n");
		
		System.out.println("Surpresa do artefato 3: " + ArtifactEval.getSurprise(objMdl, objArt3, itg) + "\n");
		
		System.out.println("Surpresa do artefato 4: " + ArtifactEval.getSurprise(objMdl, objArt4, itg) + "\n");
		
		System.out.println("Surpresa do artefato 5: " + ArtifactEval.getSurprise(objMdl, objArt5, itg) + "\n\n");
		
		System.out.println("----------------------------------------------------------------\n\n");
		
		System.out.println("Apreendendo o artefato 1!\n\n");
		
		ArtifactEval.learn(objMdl, objArt1);
		
		System.out.println("Surpresa do artefato 1: " + ArtifactEval.getSurprise(objMdl, objArt1, itg) + "\n");
		
		System.out.println("Surpresa do artefato 2: " + ArtifactEval.getSurprise(objMdl, objArt2, itg) + "\n");
		
		System.out.println("Surpresa do artefato 3: " + ArtifactEval.getSurprise(objMdl, objArt3, itg) + "\n");
		
		System.out.println("Surpresa do artefato 4: " + ArtifactEval.getSurprise(objMdl, objArt4, itg) + "\n");
		
		System.out.println("Surpresa do artefato 5: " + ArtifactEval.getSurprise(objMdl, objArt5, itg) + "\n\n");
		
		System.out.println("----------------------------------------------------------------\n\n");
		
		System.out.println("Apreendendo o artefato 2!\n\n");
		
		ArtifactEval.learn(objMdl, objArt2);
		
		System.out.println("Surpresa do artefato 1: " + ArtifactEval.getSurprise(objMdl, objArt1, itg) + "\n");
		
		System.out.println("Surpresa do artefato 2: " + ArtifactEval.getSurprise(objMdl, objArt2, itg) + "\n");
		
		System.out.println("Surpresa do artefato 3: " + ArtifactEval.getSurprise(objMdl, objArt3, itg) + "\n");
		
		System.out.println("Surpresa do artefato 4: " + ArtifactEval.getSurprise(objMdl, objArt4, itg) + "\n");
		
		System.out.println("Surpresa do artefato 5: " + ArtifactEval.getSurprise(objMdl, objArt5, itg) + "\n\n");
		
		System.out.println("----------------------------------------------------------------\n\n");
		
		System.out.println("Apreendendo o artefato 3!\n\n");
		
		ArtifactEval.learn(objMdl, objArt3);
		
		System.out.println("Surpresa do artefato 1: " + ArtifactEval.getSurprise(objMdl, objArt1, itg) + "\n");
		
		System.out.println("Surpresa do artefato 2: " + ArtifactEval.getSurprise(objMdl, objArt2, itg) + "\n");
		
		System.out.println("Surpresa do artefato 3: " + ArtifactEval.getSurprise(objMdl, objArt3, itg) + "\n");
		
		System.out.println("Surpresa do artefato 4: " + ArtifactEval.getSurprise(objMdl, objArt4, itg) + "\n");
		
		System.out.println("Surpresa do artefato 5: " + ArtifactEval.getSurprise(objMdl, objArt5, itg) + "\n\n");
		
		System.out.println("----------------------------------------------------------------\n\n");
		
		System.out.println("Apreendendo o artefato 4!\n\n");
		
		ArtifactEval.learn(objMdl, objArt4);
		
		System.out.println("Surpresa do artefato 1: " + ArtifactEval.getSurprise(objMdl, objArt1, itg) + "\n");
		
		System.out.println("Surpresa do artefato 2: " + ArtifactEval.getSurprise(objMdl, objArt2, itg) + "\n");
		
		System.out.println("Surpresa do artefato 3: " + ArtifactEval.getSurprise(objMdl, objArt3, itg) + "\n");
		
		System.out.println("Surpresa do artefato 4: " + ArtifactEval.getSurprise(objMdl, objArt4, itg) + "\n");
		
		System.out.println("Surpresa do artefato 5: " + ArtifactEval.getSurprise(objMdl, objArt5, itg) + "\n\n");
		
		System.out.println("----------------------------------------------------------------\n\n");
		
		System.out.println("Apreendendo o artefato 5!\n\n");
		
		ArtifactEval.learn(objMdl, objArt5);
		
		System.out.println("Surpresa do artefato 1: " + ArtifactEval.getSurprise(objMdl, objArt1, itg) + "\n");
		
		System.out.println("Surpresa do artefato 2: " + ArtifactEval.getSurprise(objMdl, objArt2, itg) + "\n");
		
		System.out.println("Surpresa do artefato 3: " + ArtifactEval.getSurprise(objMdl, objArt3, itg) + "\n");
		
		System.out.println("Surpresa do artefato 4: " + ArtifactEval.getSurprise(objMdl, objArt4, itg) + "\n");
		
		System.out.println("Surpresa do artefato 5: " + ArtifactEval.getSurprise(objMdl, objArt5, itg) + "\n\n");
		
		System.out.println("----------------------------------------------------------------\n\n");
		
		// Leitura dos parâmetros dos modelos bayesianos do conjunto de modelos:
		
		System.out.println("Lendo modelos iniciais (ignorando o treinamento)!\n\n");
		
		objMdl.read("modelos.dat");
		
		// Obs: a escrita e leitura correta (em termos de órdem de modelos bayesianos) é responsabilidade do usuário da biblioteca.
		//      Isto é garantido pela utilização de conjunto de modelos com mesmas distribuições entre escrita e leitura.
		
		System.out.println("Surpresa do artefato 1: " + ArtifactEval.getSurprise(objMdl, objArt1, itg) + "\n");
		
		System.out.println("Surpresa do artefato 2: " + ArtifactEval.getSurprise(objMdl, objArt2, itg) + "\n");
		
		System.out.println("Surpresa do artefato 3: " + ArtifactEval.getSurprise(objMdl, objArt3, itg) + "\n");
		
		System.out.println("Surpresa do artefato 4: " + ArtifactEval.getSurprise(objMdl, objArt4, itg) + "\n");
		
		System.out.println("Surpresa do artefato 5: " + ArtifactEval.getSurprise(objMdl, objArt5, itg) + "\n\n");
	}

}
